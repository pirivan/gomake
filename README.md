This project hosts a GNU makefile for GO language projects.
For further details see [A Makefile for Golang](https://pirivan.gitlab.io/post/golang/a-makefile-for-golang/).
