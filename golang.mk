#
# This is free and unencumbered software released into the public domain.
# See the LICENSE file for details.
# 

###############
# targets
###############
# Default directory values
BINDIR ?= $(GOPATH)/bin
CMDDIR ?= cmd
INTERNALDIR ?= internal/pkg

# Current dir with respect to $GOPATH
SRCTREE := $(subst $(GOPATH)/src/,,$(shell pwd))

# Programms (commands) to build
PROGRAMS := $(notdir $(shell ls -d $(CMDDIR)/*))
BINFILES := $(addprefix $(BINDIR)/,$(PROGRAMS))

# Internal libs to build
LIBS := $(notdir $(shell find $(INTERNALDIR) -type d | grep -v $(INTERNALDIR)$$))

# Makefile target control files
INTERNALS := $(addprefix .build_internal_, $(LIBS))
COMMANDS  := $(addprefix .build_cmd_, $(PROGRAMS))
GOTESTS := $(addsuffix _test, $(INTERNALS) $(COMMANDS))

.PHONY: commands tests
commands: $(INTERNALS) $(COMMANDS)
tests: $(GOTESTS)


###############
# internal libs
###############
define internal_build_rule
go install $(SRCTREE)/$(INTERNALDIR)/$*
@touch $@
endef

.SECONDEXPANSION:
.build_internal_%: $$(shell find $(INTERNALDIR)/$$* -type f -name *.go | grep -v _test.go$$)
	$(internal_build_rule)


###############
# commands
###############
define cmd_build_rule
go build -o $(BINDIR)/$* $(SRCTREE)/$(CMDDIR)/$*
@touch $@
endef

.build_cmd_%: $$(shell find $(CMDDIR)/$$* -type f -name *.go | grep -v _test.go$$)
	$(cmd_build_rule)


###############
# tests
###############
.build_cmd_%_test: $$(shell find $(CMDDIR)/$$* -type f -name *.go)
	go test $(SRCTREE)/$(CMDDIR)/$*
	@touch $@

.build_internal_%_test: $$(shell find $(INTERNALDIR)/$$* -type f -name *.go)
	go test $(SRCTREE)/$(INTERNALDIR)/$*
	@touch $@


###############
# other
###############
.PHONY: clean
clean:
	@rm -f $(BINFILES) .build_*
	@echo "done"
